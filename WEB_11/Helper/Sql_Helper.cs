﻿using Microsoft.Data.SqlClient;

namespace WebApplication1.helper
{
    public class Sql_Helper
    {
        
        IConfiguration config = new ConfigurationBuilder()
          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
          .Build();

        public SqlConnection Cont(SqlConnection _cont)
        {
            string connectionString = config["ConnectionStrings"].ToString();
            _cont = new SqlConnection(connectionString);
            return _cont;
        }

     
    }
}
