﻿using Microsoft.Data.SqlClient;
using System.Reflection.PortableExecutable;
using WebApplication1.helper;

namespace WEB_11.Controllers.Data
{
    public class Product
    {
        public int ProductId;
        public string ProductCode = "";
        public string ProductName = "";
        public decimal PricePerUnit;
        public int StockQty;
    }

    public class Product_Interface
    {
        SqlConnection cont = new SqlConnection();
        Sql_Helper sql_h = new Sql_Helper();
        IConfiguration config = new ConfigurationBuilder()
          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
          .Build();

        public List<Product> Get_data()
        {
            string res = "";

            cont = sql_h.Cont(cont);
            cont.Open();
            string cmd = "Select [ProductId],[ProductCode],[ProductName],[PricePerUnit],[StockQty] from Product ";
            List<Product> res_dt = new List<Product>();

            using (SqlCommand command = new SqlCommand(cmd, cont))
            {

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    List<Dictionary<string, object>> resultList = new List<Dictionary<string, object>>();
                    while (reader.Read())
                    {
                        Product dr = new Product();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            if (reader.GetName(i) == "ProductId")
                            {
                                dr.ProductId = Convert.ToInt32(reader.GetValue(i).ToString());
                            }
                            if (reader.GetName(i) == "ProductCode")
                            {
                                dr.ProductCode = reader.GetValue(i).ToString();
                            }
                            if (reader.GetName(i) == "ProductName")
                            {
                                dr.ProductName = reader.GetValue(i).ToString();
                            }
                            if (reader.GetName(i) == "PricePerUnit")
                            {
                                dr.PricePerUnit = Convert.ToDecimal(reader.GetValue(i).ToString());
                            }
                            if (reader.GetName(i) == "StockQty")
                            {
                                dr.StockQty = Convert.ToInt32(reader.GetValue(i).ToString());
                            }
                        }
                        res_dt.Add(dr);

                    }

                    reader.Close();


                }
            }
            // Perform database operations here...

            return res_dt;

        }

        public string Check_ProductCode(string ProductCode)
        {
            string res = "999";

            cont = sql_h.Cont(cont);
            cont.Open();
            string cmd = " Select Count(*) as total from Product where ProductCode ='" + ProductCode + "'";

            using (SqlCommand command = new SqlCommand(cmd, cont))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    res = reader.GetValue(0).ToString();
                    reader.Close();
                }
            }
            return res;
        }

        public string Post_Data(string ProductCode, string ProductName, decimal PricePerUnit, int StockQty)
        {
            string res = "";

            cont = sql_h.Cont(cont);
            cont.Open();

                    if (Convert.ToInt32(Check_ProductCode(ProductCode)) == 0)
                    {
                        string cmd = "INSERT INTO [dbo].[Product]([ProductCode],[ProductName],[PricePerUnit],[StockQty]) VALUES('" + ProductCode + "','" + ProductName + "'," + PricePerUnit + "," + StockQty + ")";
                        using (SqlCommand command2 = new SqlCommand(cmd, cont))
                        {
                            command2.ExecuteReader();
                            res = "Success";
                        }
                    }
                    else
                    {
                        res = "Exist";
                    }
            return res;
        }

        public string Update_Data(int ProductId, int StockQty)
        {
            string res = "";
            string cmd = "update [TTB_DB].[dbo].[Product] set StockQty = (StockQty - "+ StockQty.ToString() + ") where ProductId = "+ ProductId;

            cont = sql_h.Cont(cont);
            cont.Open();
            
            using (SqlCommand command2 = new SqlCommand(cmd, cont))
            {
                command2.ExecuteReader();
                res = "Success";
            }
            
            return res;
        }
    }
}
