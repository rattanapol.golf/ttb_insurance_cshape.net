﻿using Microsoft.AspNetCore.Mvc;
using WEB_11.Controllers.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEB_11.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        // GET: api/<ProductController>
        [HttpGet]
        public ActionResult<object> Get()
        {
            Product_Interface item_if = new Product_Interface();
            List<Product> list = new List<Product>();
            list = item_if.Get_data();
            var data = list.Select(x => new { x.ProductId, x.ProductName, x.ProductCode,x.PricePerUnit,x.StockQty }).ToList();
            return Ok(data);
        }


        // POST api/<ProductController>
        [HttpPost]
        public string Post(string Value)
        {
            string[] v = Value.Split("|");
            Product_Interface item_if = new Product_Interface();

            return item_if.Post_Data(v[0], v[1], Convert.ToDecimal(v[2].ToString()), Convert.ToInt32(v[3].ToString()));
        }

        // POST api/<ProductController>
        [HttpPatch]
        public string Patch(string Value)
        {
            Product_Interface item_if = new Product_Interface();

            string[] v = Value.Split("|");
            for (int i = 0; i < v.Length; i++) {
                if(v[i] != "")
                { 
                string[] res = v[i].Split(",");
                res[0] = res[0].Replace("CartRow_", "");
                item_if.Update_Data(Convert.ToInt32(res[0]), Convert.ToInt32(res[1]));
                }
            }
            return "";
        }

    }
}
